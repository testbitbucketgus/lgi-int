/* VMR Parameters - not expected to be customized during lgiEnhApiBs installation */
INSERT IGNORE INTO `user`.`configuration`
(`instanceName`,`serviceName`,`internal`,`parameterName`,`parameterValue`,`parameterType`,`version`)
VALUES
('any','any','0','cdvrDsBaseUrl','http://localhost:8080/cDVR','stringType','0');
INSERT INTO `user`.`configuration`
(`instanceName`,`serviceName`,`internal`,`parameterName`,`parameterValue`,`parameterType`,`version`)
VALUES
('any','any',0,'epgDsBaseUrl','http://localhost:8082/epg','stringType',0);
INSERT INTO `user`.`configuration`
(`instanceName`,`serviceName`,`internal`,`parameterName`,`parameterValue`,`parameterType`,`version`)
VALUES
('any','any',0,'epgBsBaseUrl','http://localhost:8083/epgBs','stringType',0);


/* VMR Parameters - Please check values and customize values (in this script, or records already existing in DB table user.configuration) */
INSERT IGNORE INTO `user`.`configuration`
(`instanceName`,`serviceName`,`internal`,`parameterName`,`parameterValue`,`parameterType`,`version`)
VALUES
('any','lgiEnhApiBs','0','vrmPageSize','4000','integerType','0');
INSERT IGNORE INTO `user`.`configuration`
(`instanceName`,`serviceName`,`internal`,`parameterName`,`parameterValue`,`parameterType`,`version`)
VALUES
('any','lgiEnhApiBs','0','abrStatus8Enabled','true','booleanType','0');
INSERT IGNORE INTO `user`.`configuration`
(`instanceName`,`serviceName`,`internal`,`parameterName`,`parameterValue`,`parameterType`,`version`)
VALUES
('any','lgiEnhApiBs','0','rerunsEnabled','false','booleanType','0');
INSERT IGNORE INTO `user`.`configuration`
(`instanceName`,`serviceName`,`internal`,`parameterName`,`parameterValue`,`parameterType`,`version`)
VALUES
('any','lgiEnhApiBs','0','epgCacheRefreshTimeSec','300','integerType','0');
INSERT IGNORE INTO `user`.`configuration`
(`instanceName`,`serviceName`,`internal`,`parameterName`,`parameterValue`,`parameterType`,`version`)
VALUES
('any','lgiEnhApiBs','0','epgCacheChannelsPerRequest','10','integerType','0');
INSERT IGNORE INTO `user`.`configuration`
(`instanceName`,`serviceName`,`internal`,`parameterName`,`parameterValue`,`parameterType`,`version`)
VALUES
('any','lgiEnhApiBs','0','epgCachePageSize','50000','integerType','0');
